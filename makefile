#
#
# Author: Teunis van Beelen
#
# email: teuniz@protonmail.com
#
#

CC = gcc
CFLAGS = -pthread -O2 -std=gnu11 -Wall -Wextra -Wshadow -Wformat-nonliteral -Wformat-security \
         -Wtype-limits -Wfatal-errors -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE
LDFLAGS =
LDLIBS = -lm -lpthread

objects = obj/main.o obj/edflib.o \
          obj/filtering.o obj/multi_stage.o obj/polyfilt.o obj/remez_lp.o obj/smarc.o obj/stage_impl.o

all: edf-resampler

edf-resampler : $(objects)
	$(CC) $(objects) -o edf-resampler  $(LDLIBS)

obj/main.o : main.c edflib.h third_party/smarc/smarc.h
	$(CC) $(CFLAGS) -c main.c -o obj/main.o

obj/edflib.o : edflib.h edflib.c
	$(CC) $(CFLAGS) -c edflib.c -o obj/edflib.o

obj/filtering.o : third_party/smarc/filtering.h third_party/smarc/filtering.c
	$(CC) $(CFLAGS) -c third_party/smarc/filtering.c -o obj/filtering.o

obj/multi_stage.o : third_party/smarc/multi_stage.h third_party/smarc/multi_stage.c
	$(CC) $(CFLAGS) -c third_party/smarc/multi_stage.c -o obj/multi_stage.o

obj/polyfilt.o : third_party/smarc/polyfilt.h third_party/smarc/polyfilt.c
	$(CC) $(CFLAGS) -c third_party/smarc/polyfilt.c -o obj/polyfilt.o

obj/remez_lp.o : third_party/smarc/remez_lp.h third_party/smarc/remez_lp.c
	$(CC) $(CFLAGS) -c third_party/smarc/remez_lp.c -o obj/remez_lp.o

obj/smarc.o : third_party/smarc/smarc.h third_party/smarc/smarc.c
	$(CC) $(CFLAGS) -c third_party/smarc/smarc.c -o obj/smarc.o

obj/stage_impl.o : third_party/smarc/stage_impl.h third_party/smarc/stage_impl.c
	$(CC) $(CFLAGS) -c third_party/smarc/stage_impl.c -o obj/stage_impl.o

clean :
	$(RM) edf-resampler $(objects)

#
#
#
#





